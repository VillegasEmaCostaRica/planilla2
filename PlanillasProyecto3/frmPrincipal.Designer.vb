﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPrincipal
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.DatosEmpleadoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.IngresarDatosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ConsultarDatosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ActualizarDatosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PuestosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.IngresarPuestosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ConsultarPuestosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AjustesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.IngresarAjustesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ConsultarAjustesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DeduccucionesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.IngresarDeduccionesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ConsultarDedueccionesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PlanillaMensualToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.IngresarPlanillaMensualToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ConsultarPlanillaMensualToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PlanillaBisemanalToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.IngresarPlanillaBisemanalToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ConsultarPlanillaBisemanalToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ReporteríaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EliminarDatosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.MenuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = Global.PlanillasProyecto3.My.Resources.Resources._0412_avvillas_cambia_formato_pago_planilla_asistida
        Me.PictureBox1.Location = New System.Drawing.Point(58, 122)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(174, 164)
        Me.PictureBox1.TabIndex = 0
        Me.PictureBox1.TabStop = False
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 378)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(705, 22)
        Me.StatusStrip1.TabIndex = 1
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.DatosEmpleadoToolStripMenuItem, Me.PuestosToolStripMenuItem, Me.AjustesToolStripMenuItem, Me.DeduccucionesToolStripMenuItem, Me.PlanillaMensualToolStripMenuItem, Me.PlanillaBisemanalToolStripMenuItem, Me.ReporteríaToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(705, 24)
        Me.MenuStrip1.TabIndex = 2
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'DatosEmpleadoToolStripMenuItem
        '
        Me.DatosEmpleadoToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.IngresarDatosToolStripMenuItem, Me.ConsultarDatosToolStripMenuItem, Me.ActualizarDatosToolStripMenuItem, Me.EliminarDatosToolStripMenuItem})
        Me.DatosEmpleadoToolStripMenuItem.Name = "DatosEmpleadoToolStripMenuItem"
        Me.DatosEmpleadoToolStripMenuItem.Size = New System.Drawing.Size(105, 20)
        Me.DatosEmpleadoToolStripMenuItem.Text = "&Datos Empleado"
        '
        'IngresarDatosToolStripMenuItem
        '
        Me.IngresarDatosToolStripMenuItem.Name = "IngresarDatosToolStripMenuItem"
        Me.IngresarDatosToolStripMenuItem.Size = New System.Drawing.Size(159, 22)
        Me.IngresarDatosToolStripMenuItem.Text = "&Ingresar Datos"
        '
        'ConsultarDatosToolStripMenuItem
        '
        Me.ConsultarDatosToolStripMenuItem.Name = "ConsultarDatosToolStripMenuItem"
        Me.ConsultarDatosToolStripMenuItem.Size = New System.Drawing.Size(159, 22)
        Me.ConsultarDatosToolStripMenuItem.Text = "&Consultar Datos"
        '
        'ActualizarDatosToolStripMenuItem
        '
        Me.ActualizarDatosToolStripMenuItem.Name = "ActualizarDatosToolStripMenuItem"
        Me.ActualizarDatosToolStripMenuItem.Size = New System.Drawing.Size(159, 22)
        Me.ActualizarDatosToolStripMenuItem.Text = "&Actualizar Datos"
        '
        'PuestosToolStripMenuItem
        '
        Me.PuestosToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.IngresarPuestosToolStripMenuItem, Me.ConsultarPuestosToolStripMenuItem})
        Me.PuestosToolStripMenuItem.Name = "PuestosToolStripMenuItem"
        Me.PuestosToolStripMenuItem.Size = New System.Drawing.Size(60, 20)
        Me.PuestosToolStripMenuItem.Text = "&Puestos"
        '
        'IngresarPuestosToolStripMenuItem
        '
        Me.IngresarPuestosToolStripMenuItem.Name = "IngresarPuestosToolStripMenuItem"
        Me.IngresarPuestosToolStripMenuItem.Size = New System.Drawing.Size(169, 22)
        Me.IngresarPuestosToolStripMenuItem.Text = "&Ingresar Puestos"
        '
        'ConsultarPuestosToolStripMenuItem
        '
        Me.ConsultarPuestosToolStripMenuItem.Name = "ConsultarPuestosToolStripMenuItem"
        Me.ConsultarPuestosToolStripMenuItem.Size = New System.Drawing.Size(169, 22)
        Me.ConsultarPuestosToolStripMenuItem.Text = "&Consultar Puestos"
        '
        'AjustesToolStripMenuItem
        '
        Me.AjustesToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.IngresarAjustesToolStripMenuItem, Me.ConsultarAjustesToolStripMenuItem})
        Me.AjustesToolStripMenuItem.Name = "AjustesToolStripMenuItem"
        Me.AjustesToolStripMenuItem.Size = New System.Drawing.Size(57, 20)
        Me.AjustesToolStripMenuItem.Text = "&Ajustes"
        '
        'IngresarAjustesToolStripMenuItem
        '
        Me.IngresarAjustesToolStripMenuItem.Name = "IngresarAjustesToolStripMenuItem"
        Me.IngresarAjustesToolStripMenuItem.Size = New System.Drawing.Size(166, 22)
        Me.IngresarAjustesToolStripMenuItem.Text = "&Ingresar Ajustes"
        '
        'ConsultarAjustesToolStripMenuItem
        '
        Me.ConsultarAjustesToolStripMenuItem.Name = "ConsultarAjustesToolStripMenuItem"
        Me.ConsultarAjustesToolStripMenuItem.Size = New System.Drawing.Size(166, 22)
        Me.ConsultarAjustesToolStripMenuItem.Text = "&Consultar Ajustes"
        '
        'DeduccucionesToolStripMenuItem
        '
        Me.DeduccucionesToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.IngresarDeduccionesToolStripMenuItem, Me.ConsultarDedueccionesToolStripMenuItem})
        Me.DeduccucionesToolStripMenuItem.Name = "DeduccucionesToolStripMenuItem"
        Me.DeduccucionesToolStripMenuItem.Size = New System.Drawing.Size(87, 20)
        Me.DeduccucionesToolStripMenuItem.Text = "&Deducciones"
        '
        'IngresarDeduccionesToolStripMenuItem
        '
        Me.IngresarDeduccionesToolStripMenuItem.Name = "IngresarDeduccionesToolStripMenuItem"
        Me.IngresarDeduccionesToolStripMenuItem.Size = New System.Drawing.Size(196, 22)
        Me.IngresarDeduccionesToolStripMenuItem.Text = "&Ingresar Deducciones"
        '
        'ConsultarDedueccionesToolStripMenuItem
        '
        Me.ConsultarDedueccionesToolStripMenuItem.Name = "ConsultarDedueccionesToolStripMenuItem"
        Me.ConsultarDedueccionesToolStripMenuItem.Size = New System.Drawing.Size(196, 22)
        Me.ConsultarDedueccionesToolStripMenuItem.Text = "&Consultar Deducciones"
        '
        'PlanillaMensualToolStripMenuItem
        '
        Me.PlanillaMensualToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.IngresarPlanillaMensualToolStripMenuItem, Me.ConsultarPlanillaMensualToolStripMenuItem})
        Me.PlanillaMensualToolStripMenuItem.Name = "PlanillaMensualToolStripMenuItem"
        Me.PlanillaMensualToolStripMenuItem.Size = New System.Drawing.Size(105, 20)
        Me.PlanillaMensualToolStripMenuItem.Text = "&Planilla Mensual"
        '
        'IngresarPlanillaMensualToolStripMenuItem
        '
        Me.IngresarPlanillaMensualToolStripMenuItem.Name = "IngresarPlanillaMensualToolStripMenuItem"
        Me.IngresarPlanillaMensualToolStripMenuItem.Size = New System.Drawing.Size(214, 22)
        Me.IngresarPlanillaMensualToolStripMenuItem.Text = "&Ingresar Planilla Mensual"
        '
        'ConsultarPlanillaMensualToolStripMenuItem
        '
        Me.ConsultarPlanillaMensualToolStripMenuItem.Name = "ConsultarPlanillaMensualToolStripMenuItem"
        Me.ConsultarPlanillaMensualToolStripMenuItem.Size = New System.Drawing.Size(214, 22)
        Me.ConsultarPlanillaMensualToolStripMenuItem.Text = "&Consultar Planilla Mensual"
        '
        'PlanillaBisemanalToolStripMenuItem
        '
        Me.PlanillaBisemanalToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.IngresarPlanillaBisemanalToolStripMenuItem, Me.ConsultarPlanillaBisemanalToolStripMenuItem})
        Me.PlanillaBisemanalToolStripMenuItem.Name = "PlanillaBisemanalToolStripMenuItem"
        Me.PlanillaBisemanalToolStripMenuItem.Size = New System.Drawing.Size(114, 20)
        Me.PlanillaBisemanalToolStripMenuItem.Text = "&Planilla Bisemanal"
        '
        'IngresarPlanillaBisemanalToolStripMenuItem
        '
        Me.IngresarPlanillaBisemanalToolStripMenuItem.Name = "IngresarPlanillaBisemanalToolStripMenuItem"
        Me.IngresarPlanillaBisemanalToolStripMenuItem.Size = New System.Drawing.Size(223, 22)
        Me.IngresarPlanillaBisemanalToolStripMenuItem.Text = "&Ingresar Planilla Bisemanal"
        '
        'ConsultarPlanillaBisemanalToolStripMenuItem
        '
        Me.ConsultarPlanillaBisemanalToolStripMenuItem.Name = "ConsultarPlanillaBisemanalToolStripMenuItem"
        Me.ConsultarPlanillaBisemanalToolStripMenuItem.Size = New System.Drawing.Size(223, 22)
        Me.ConsultarPlanillaBisemanalToolStripMenuItem.Text = "&Consultar Planilla Bisemanal"
        '
        'ReporteríaToolStripMenuItem
        '
        Me.ReporteríaToolStripMenuItem.Name = "ReporteríaToolStripMenuItem"
        Me.ReporteríaToolStripMenuItem.Size = New System.Drawing.Size(73, 20)
        Me.ReporteríaToolStripMenuItem.Text = "&Reportería"
        '
        'EliminarDatosToolStripMenuItem
        '
        Me.EliminarDatosToolStripMenuItem.Name = "EliminarDatosToolStripMenuItem"
        Me.EliminarDatosToolStripMenuItem.Size = New System.Drawing.Size(159, 22)
        Me.EliminarDatosToolStripMenuItem.Text = "&Eliminar Datos"
        '
        'frmPrincipal
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = Global.PlanillasProyecto3.My.Resources.Resources.planillas_de_pago
        Me.ClientSize = New System.Drawing.Size(705, 400)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.MenuStrip1)
        Me.Controls.Add(Me.PictureBox1)
        Me.MainMenuStrip = Me.MenuStrip1
        Me.MaximizeBox = False
        Me.Name = "frmPrincipal"
        Me.Text = "frmPrincipal"
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents DatosEmpleadoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents IngresarDatosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ConsultarDatosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PuestosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents IngresarPuestosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ConsultarPuestosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AjustesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents IngresarAjustesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ConsultarAjustesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DeduccucionesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents IngresarDeduccionesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ConsultarDedueccionesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PlanillaMensualToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents IngresarPlanillaMensualToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ConsultarPlanillaMensualToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PlanillaBisemanalToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents IngresarPlanillaBisemanalToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ConsultarPlanillaBisemanalToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ReporteríaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ActualizarDatosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EliminarDatosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
End Class
