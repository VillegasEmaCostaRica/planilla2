﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmDatosEmpleado
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim _1__APELLIDOLabel As System.Windows.Forms.Label
        Dim _2__APELLIDOLabel As System.Windows.Forms.Label
        Dim NOMBRELabel As System.Windows.Forms.Label
        Dim E_MAILLabel As System.Windows.Forms.Label
        Dim CODPUESTOLabel As System.Windows.Forms.Label
        Dim N__CONYUGESLabel As System.Windows.Forms.Label
        Dim N__HIJOSLabel As System.Windows.Forms.Label
        Dim GRADO_ACADEMICOLabel As System.Windows.Forms.Label
        Dim BANCO_A_DEPOSITARLabel As System.Windows.Forms.Label
        Dim NoCUENTALabel As System.Windows.Forms.Label
        Dim TelefonoLabel As System.Windows.Forms.Label
        Dim CelularLabel As System.Windows.Forms.Label
        Dim DireccionLabel As System.Windows.Forms.Label
        Dim NoCEDULALabel As System.Windows.Forms.Label
        Dim FECHA_INGRESOLabel1 As System.Windows.Forms.Label
        Me.Planilla2DataSet = New PlanillasProyecto3.Planilla2DataSet()
        Me.DatosPersonalesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DatosPersonalesTableAdapter = New PlanillasProyecto3.Planilla2DataSetTableAdapters.DatosPersonalesTableAdapter()
        Me.TableAdapterManager = New PlanillasProyecto3.Planilla2DataSetTableAdapters.TableAdapterManager()
        Me.NoCEDULATextBox = New System.Windows.Forms.TextBox()
        Me._1__APELLIDOTextBox = New System.Windows.Forms.TextBox()
        Me._2__APELLIDOTextBox = New System.Windows.Forms.TextBox()
        Me.NOMBRETextBox = New System.Windows.Forms.TextBox()
        Me.E_MAILTextBox = New System.Windows.Forms.TextBox()
        Me.CODPUESTOTextBox = New System.Windows.Forms.TextBox()
        Me.N__CONYUGESCheckBox = New System.Windows.Forms.CheckBox()
        Me.N__HIJOSTextBox = New System.Windows.Forms.TextBox()
        Me.GRADO_ACADEMICOTextBox = New System.Windows.Forms.TextBox()
        Me.BANCO_A_DEPOSITARTextBox = New System.Windows.Forms.TextBox()
        Me.NoCUENTATextBox = New System.Windows.Forms.TextBox()
        Me.TelefonoTextBox = New System.Windows.Forms.TextBox()
        Me.CelularTextBox = New System.Windows.Forms.TextBox()
        Me.DireccionTextBox = New System.Windows.Forms.TextBox()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.FECHA_INGRESODateTimePicker = New System.Windows.Forms.DateTimePicker()
        _1__APELLIDOLabel = New System.Windows.Forms.Label()
        _2__APELLIDOLabel = New System.Windows.Forms.Label()
        NOMBRELabel = New System.Windows.Forms.Label()
        E_MAILLabel = New System.Windows.Forms.Label()
        CODPUESTOLabel = New System.Windows.Forms.Label()
        N__CONYUGESLabel = New System.Windows.Forms.Label()
        N__HIJOSLabel = New System.Windows.Forms.Label()
        GRADO_ACADEMICOLabel = New System.Windows.Forms.Label()
        BANCO_A_DEPOSITARLabel = New System.Windows.Forms.Label()
        NoCUENTALabel = New System.Windows.Forms.Label()
        TelefonoLabel = New System.Windows.Forms.Label()
        CelularLabel = New System.Windows.Forms.Label()
        DireccionLabel = New System.Windows.Forms.Label()
        NoCEDULALabel = New System.Windows.Forms.Label()
        FECHA_INGRESOLabel1 = New System.Windows.Forms.Label()
        CType(Me.Planilla2DataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DatosPersonalesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        '_1__APELLIDOLabel
        '
        _1__APELLIDOLabel.AutoSize = True
        _1__APELLIDOLabel.Location = New System.Drawing.Point(109, 97)
        _1__APELLIDOLabel.Name = "_1__APELLIDOLabel"
        _1__APELLIDOLabel.Size = New System.Drawing.Size(75, 13)
        _1__APELLIDOLabel.TabIndex = 3
        _1__APELLIDOLabel.Text = "1° APELLIDO:"
        '
        '_2__APELLIDOLabel
        '
        _2__APELLIDOLabel.AutoSize = True
        _2__APELLIDOLabel.Location = New System.Drawing.Point(109, 123)
        _2__APELLIDOLabel.Name = "_2__APELLIDOLabel"
        _2__APELLIDOLabel.Size = New System.Drawing.Size(75, 13)
        _2__APELLIDOLabel.TabIndex = 5
        _2__APELLIDOLabel.Text = "2° APELLIDO:"
        '
        'NOMBRELabel
        '
        NOMBRELabel.AutoSize = True
        NOMBRELabel.Location = New System.Drawing.Point(109, 149)
        NOMBRELabel.Name = "NOMBRELabel"
        NOMBRELabel.Size = New System.Drawing.Size(57, 13)
        NOMBRELabel.TabIndex = 7
        NOMBRELabel.Text = "NOMBRE:"
        '
        'E_MAILLabel
        '
        E_MAILLabel.AutoSize = True
        E_MAILLabel.Location = New System.Drawing.Point(109, 201)
        E_MAILLabel.Name = "E_MAILLabel"
        E_MAILLabel.Size = New System.Drawing.Size(45, 13)
        E_MAILLabel.TabIndex = 11
        E_MAILLabel.Text = "E-MAIL:"
        '
        'CODPUESTOLabel
        '
        CODPUESTOLabel.AutoSize = True
        CODPUESTOLabel.Location = New System.Drawing.Point(109, 227)
        CODPUESTOLabel.Name = "CODPUESTOLabel"
        CODPUESTOLabel.Size = New System.Drawing.Size(77, 13)
        CODPUESTOLabel.TabIndex = 13
        CODPUESTOLabel.Text = "CODPUESTO:"
        '
        'N__CONYUGESLabel
        '
        N__CONYUGESLabel.AutoSize = True
        N__CONYUGESLabel.Location = New System.Drawing.Point(109, 285)
        N__CONYUGESLabel.Name = "N__CONYUGESLabel"
        N__CONYUGESLabel.Size = New System.Drawing.Size(54, 13)
        N__CONYUGESLabel.TabIndex = 19
        N__CONYUGESLabel.Text = "CASADO:"
        '
        'N__HIJOSLabel
        '
        N__HIJOSLabel.AutoSize = True
        N__HIJOSLabel.Location = New System.Drawing.Point(109, 335)
        N__HIJOSLabel.Name = "N__HIJOSLabel"
        N__HIJOSLabel.Size = New System.Drawing.Size(56, 13)
        N__HIJOSLabel.TabIndex = 21
        N__HIJOSLabel.Text = "N° HIJOS:"
        '
        'GRADO_ACADEMICOLabel
        '
        GRADO_ACADEMICOLabel.AutoSize = True
        GRADO_ACADEMICOLabel.Location = New System.Drawing.Point(109, 361)
        GRADO_ACADEMICOLabel.Name = "GRADO_ACADEMICOLabel"
        GRADO_ACADEMICOLabel.Size = New System.Drawing.Size(115, 13)
        GRADO_ACADEMICOLabel.TabIndex = 23
        GRADO_ACADEMICOLabel.Text = "GRADO ACADEMICO:"
        '
        'BANCO_A_DEPOSITARLabel
        '
        BANCO_A_DEPOSITARLabel.AutoSize = True
        BANCO_A_DEPOSITARLabel.Location = New System.Drawing.Point(109, 387)
        BANCO_A_DEPOSITARLabel.Name = "BANCO_A_DEPOSITARLabel"
        BANCO_A_DEPOSITARLabel.Size = New System.Drawing.Size(122, 13)
        BANCO_A_DEPOSITARLabel.TabIndex = 25
        BANCO_A_DEPOSITARLabel.Text = "BANCO A DEPOSITAR:"
        '
        'NoCUENTALabel
        '
        NoCUENTALabel.AutoSize = True
        NoCUENTALabel.Location = New System.Drawing.Point(109, 413)
        NoCUENTALabel.Name = "NoCUENTALabel"
        NoCUENTALabel.Size = New System.Drawing.Size(71, 13)
        NoCUENTALabel.TabIndex = 27
        NoCUENTALabel.Text = "No CUENTA:"
        '
        'TelefonoLabel
        '
        TelefonoLabel.AutoSize = True
        TelefonoLabel.Location = New System.Drawing.Point(109, 440)
        TelefonoLabel.Name = "TelefonoLabel"
        TelefonoLabel.Size = New System.Drawing.Size(52, 13)
        TelefonoLabel.TabIndex = 29
        TelefonoLabel.Text = "Telefono:"
        '
        'CelularLabel
        '
        CelularLabel.AutoSize = True
        CelularLabel.Location = New System.Drawing.Point(109, 465)
        CelularLabel.Name = "CelularLabel"
        CelularLabel.Size = New System.Drawing.Size(42, 13)
        CelularLabel.TabIndex = 31
        CelularLabel.Text = "Celular:"
        '
        'DireccionLabel
        '
        DireccionLabel.AutoSize = True
        DireccionLabel.Location = New System.Drawing.Point(109, 491)
        DireccionLabel.Name = "DireccionLabel"
        DireccionLabel.Size = New System.Drawing.Size(55, 13)
        DireccionLabel.TabIndex = 33
        DireccionLabel.Text = "Direccion:"
        '
        'NoCEDULALabel
        '
        NoCEDULALabel.AutoSize = True
        NoCEDULALabel.Location = New System.Drawing.Point(109, 71)
        NoCEDULALabel.Name = "NoCEDULALabel"
        NoCEDULALabel.Size = New System.Drawing.Size(70, 13)
        NoCEDULALabel.TabIndex = 1
        NoCEDULALabel.Text = "No CEDULA:"
        '
        'FECHA_INGRESOLabel1
        '
        FECHA_INGRESOLabel1.AutoSize = True
        FECHA_INGRESOLabel1.Location = New System.Drawing.Point(109, 260)
        FECHA_INGRESOLabel1.Name = "FECHA_INGRESOLabel1"
        FECHA_INGRESOLabel1.Size = New System.Drawing.Size(97, 13)
        FECHA_INGRESOLabel1.TabIndex = 35
        FECHA_INGRESOLabel1.Text = "FECHA INGRESO:"
        '
        'Planilla2DataSet
        '
        Me.Planilla2DataSet.DataSetName = "Planilla2DataSet"
        Me.Planilla2DataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'DatosPersonalesBindingSource
        '
        Me.DatosPersonalesBindingSource.DataMember = "DatosPersonales"
        Me.DatosPersonalesBindingSource.DataSource = Me.Planilla2DataSet
        '
        'DatosPersonalesTableAdapter
        '
        Me.DatosPersonalesTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.AjustesTableAdapter = Nothing
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.DatosPersonalesTableAdapter = Me.DatosPersonalesTableAdapter
        Me.TableAdapterManager.DeduccionesTableAdapter = Nothing
        Me.TableAdapterManager.PlanillaBisemanalTableAdapter = Nothing
        Me.TableAdapterManager.PlanillaMensualTableAdapter = Nothing
        Me.TableAdapterManager.PuestosTableAdapter = Nothing
        Me.TableAdapterManager.UpdateOrder = PlanillasProyecto3.Planilla2DataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        '
        'NoCEDULATextBox
        '
        Me.NoCEDULATextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DatosPersonalesBindingSource, "NoCEDULA", True))
        Me.NoCEDULATextBox.Location = New System.Drawing.Point(237, 68)
        Me.NoCEDULATextBox.Name = "NoCEDULATextBox"
        Me.NoCEDULATextBox.Size = New System.Drawing.Size(104, 20)
        Me.NoCEDULATextBox.TabIndex = 2
        '
        '_1__APELLIDOTextBox
        '
        Me._1__APELLIDOTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DatosPersonalesBindingSource, "1° APELLIDO", True))
        Me._1__APELLIDOTextBox.Location = New System.Drawing.Point(237, 94)
        Me._1__APELLIDOTextBox.Name = "_1__APELLIDOTextBox"
        Me._1__APELLIDOTextBox.Size = New System.Drawing.Size(104, 20)
        Me._1__APELLIDOTextBox.TabIndex = 4
        '
        '_2__APELLIDOTextBox
        '
        Me._2__APELLIDOTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DatosPersonalesBindingSource, "2° APELLIDO", True))
        Me._2__APELLIDOTextBox.Location = New System.Drawing.Point(237, 120)
        Me._2__APELLIDOTextBox.Name = "_2__APELLIDOTextBox"
        Me._2__APELLIDOTextBox.Size = New System.Drawing.Size(104, 20)
        Me._2__APELLIDOTextBox.TabIndex = 6
        '
        'NOMBRETextBox
        '
        Me.NOMBRETextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DatosPersonalesBindingSource, "NOMBRE", True))
        Me.NOMBRETextBox.Location = New System.Drawing.Point(237, 146)
        Me.NOMBRETextBox.Name = "NOMBRETextBox"
        Me.NOMBRETextBox.Size = New System.Drawing.Size(104, 20)
        Me.NOMBRETextBox.TabIndex = 8
        '
        'E_MAILTextBox
        '
        Me.E_MAILTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DatosPersonalesBindingSource, "E-MAIL", True))
        Me.E_MAILTextBox.Location = New System.Drawing.Point(237, 198)
        Me.E_MAILTextBox.Name = "E_MAILTextBox"
        Me.E_MAILTextBox.Size = New System.Drawing.Size(104, 20)
        Me.E_MAILTextBox.TabIndex = 12
        '
        'CODPUESTOTextBox
        '
        Me.CODPUESTOTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DatosPersonalesBindingSource, "CODPUESTO", True))
        Me.CODPUESTOTextBox.Location = New System.Drawing.Point(237, 224)
        Me.CODPUESTOTextBox.Name = "CODPUESTOTextBox"
        Me.CODPUESTOTextBox.Size = New System.Drawing.Size(104, 20)
        Me.CODPUESTOTextBox.TabIndex = 14
        '
        'N__CONYUGESCheckBox
        '
        Me.N__CONYUGESCheckBox.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.DatosPersonalesBindingSource, "N° CONYUGES", True))
        Me.N__CONYUGESCheckBox.Location = New System.Drawing.Point(237, 280)
        Me.N__CONYUGESCheckBox.Name = "N__CONYUGESCheckBox"
        Me.N__CONYUGESCheckBox.Size = New System.Drawing.Size(104, 24)
        Me.N__CONYUGESCheckBox.TabIndex = 20
        Me.N__CONYUGESCheckBox.Text = "Si"
        Me.N__CONYUGESCheckBox.UseVisualStyleBackColor = True
        '
        'N__HIJOSTextBox
        '
        Me.N__HIJOSTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DatosPersonalesBindingSource, "N° HIJOS", True))
        Me.N__HIJOSTextBox.Location = New System.Drawing.Point(237, 332)
        Me.N__HIJOSTextBox.Name = "N__HIJOSTextBox"
        Me.N__HIJOSTextBox.Size = New System.Drawing.Size(104, 20)
        Me.N__HIJOSTextBox.TabIndex = 22
        '
        'GRADO_ACADEMICOTextBox
        '
        Me.GRADO_ACADEMICOTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DatosPersonalesBindingSource, "GRADO ACADEMICO", True))
        Me.GRADO_ACADEMICOTextBox.Location = New System.Drawing.Point(237, 358)
        Me.GRADO_ACADEMICOTextBox.Name = "GRADO_ACADEMICOTextBox"
        Me.GRADO_ACADEMICOTextBox.Size = New System.Drawing.Size(104, 20)
        Me.GRADO_ACADEMICOTextBox.TabIndex = 24
        '
        'BANCO_A_DEPOSITARTextBox
        '
        Me.BANCO_A_DEPOSITARTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DatosPersonalesBindingSource, "BANCO A DEPOSITAR", True))
        Me.BANCO_A_DEPOSITARTextBox.Location = New System.Drawing.Point(237, 384)
        Me.BANCO_A_DEPOSITARTextBox.Name = "BANCO_A_DEPOSITARTextBox"
        Me.BANCO_A_DEPOSITARTextBox.Size = New System.Drawing.Size(104, 20)
        Me.BANCO_A_DEPOSITARTextBox.TabIndex = 26
        '
        'NoCUENTATextBox
        '
        Me.NoCUENTATextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DatosPersonalesBindingSource, "NoCUENTA", True))
        Me.NoCUENTATextBox.Location = New System.Drawing.Point(237, 410)
        Me.NoCUENTATextBox.Name = "NoCUENTATextBox"
        Me.NoCUENTATextBox.Size = New System.Drawing.Size(104, 20)
        Me.NoCUENTATextBox.TabIndex = 28
        '
        'TelefonoTextBox
        '
        Me.TelefonoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DatosPersonalesBindingSource, "Telefono", True))
        Me.TelefonoTextBox.Location = New System.Drawing.Point(237, 437)
        Me.TelefonoTextBox.Name = "TelefonoTextBox"
        Me.TelefonoTextBox.Size = New System.Drawing.Size(104, 20)
        Me.TelefonoTextBox.TabIndex = 30
        '
        'CelularTextBox
        '
        Me.CelularTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DatosPersonalesBindingSource, "Celular", True))
        Me.CelularTextBox.Location = New System.Drawing.Point(237, 462)
        Me.CelularTextBox.Name = "CelularTextBox"
        Me.CelularTextBox.Size = New System.Drawing.Size(104, 20)
        Me.CelularTextBox.TabIndex = 32
        '
        'DireccionTextBox
        '
        Me.DireccionTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DatosPersonalesBindingSource, "Direccion", True))
        Me.DireccionTextBox.Location = New System.Drawing.Point(237, 488)
        Me.DireccionTextBox.Name = "DireccionTextBox"
        Me.DireccionTextBox.Size = New System.Drawing.Size(104, 20)
        Me.DireccionTextBox.TabIndex = 34
        '
        'btnGuardar
        '
        Me.btnGuardar.Location = New System.Drawing.Point(465, 269)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(75, 23)
        Me.btnGuardar.TabIndex = 35
        Me.btnGuardar.Text = "&Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = True
        '
        'FECHA_INGRESODateTimePicker
        '
        Me.FECHA_INGRESODateTimePicker.DataBindings.Add(New System.Windows.Forms.Binding("Value", Me.DatosPersonalesBindingSource, "FECHA INGRESO", True))
        Me.FECHA_INGRESODateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.FECHA_INGRESODateTimePicker.Location = New System.Drawing.Point(237, 254)
        Me.FECHA_INGRESODateTimePicker.Name = "FECHA_INGRESODateTimePicker"
        Me.FECHA_INGRESODateTimePicker.Size = New System.Drawing.Size(104, 20)
        Me.FECHA_INGRESODateTimePicker.TabIndex = 36
        '
        'frmDatosEmpleado
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(691, 664)
        Me.Controls.Add(FECHA_INGRESOLabel1)
        Me.Controls.Add(Me.FECHA_INGRESODateTimePicker)
        Me.Controls.Add(Me.btnGuardar)
        Me.Controls.Add(NoCEDULALabel)
        Me.Controls.Add(Me.NoCEDULATextBox)
        Me.Controls.Add(_1__APELLIDOLabel)
        Me.Controls.Add(Me._1__APELLIDOTextBox)
        Me.Controls.Add(_2__APELLIDOLabel)
        Me.Controls.Add(Me._2__APELLIDOTextBox)
        Me.Controls.Add(NOMBRELabel)
        Me.Controls.Add(Me.NOMBRETextBox)
        Me.Controls.Add(E_MAILLabel)
        Me.Controls.Add(Me.E_MAILTextBox)
        Me.Controls.Add(CODPUESTOLabel)
        Me.Controls.Add(Me.CODPUESTOTextBox)
        Me.Controls.Add(N__CONYUGESLabel)
        Me.Controls.Add(Me.N__CONYUGESCheckBox)
        Me.Controls.Add(N__HIJOSLabel)
        Me.Controls.Add(Me.N__HIJOSTextBox)
        Me.Controls.Add(GRADO_ACADEMICOLabel)
        Me.Controls.Add(Me.GRADO_ACADEMICOTextBox)
        Me.Controls.Add(BANCO_A_DEPOSITARLabel)
        Me.Controls.Add(Me.BANCO_A_DEPOSITARTextBox)
        Me.Controls.Add(NoCUENTALabel)
        Me.Controls.Add(Me.NoCUENTATextBox)
        Me.Controls.Add(TelefonoLabel)
        Me.Controls.Add(Me.TelefonoTextBox)
        Me.Controls.Add(CelularLabel)
        Me.Controls.Add(Me.CelularTextBox)
        Me.Controls.Add(DireccionLabel)
        Me.Controls.Add(Me.DireccionTextBox)
        Me.Name = "frmDatosEmpleado"
        Me.Text = " frmDatosEmpleado"
        CType(Me.Planilla2DataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DatosPersonalesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Planilla2DataSet As PlanillasProyecto3.Planilla2DataSet
    Friend WithEvents DatosPersonalesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DatosPersonalesTableAdapter As PlanillasProyecto3.Planilla2DataSetTableAdapters.DatosPersonalesTableAdapter
    Friend WithEvents TableAdapterManager As PlanillasProyecto3.Planilla2DataSetTableAdapters.TableAdapterManager
    Friend WithEvents NoCEDULATextBox As System.Windows.Forms.TextBox
    Friend WithEvents _1__APELLIDOTextBox As System.Windows.Forms.TextBox
    Friend WithEvents _2__APELLIDOTextBox As System.Windows.Forms.TextBox
    Friend WithEvents NOMBRETextBox As System.Windows.Forms.TextBox
    Friend WithEvents E_MAILTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CODPUESTOTextBox As System.Windows.Forms.TextBox
    Friend WithEvents N__CONYUGESCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents N__HIJOSTextBox As System.Windows.Forms.TextBox
    Friend WithEvents GRADO_ACADEMICOTextBox As System.Windows.Forms.TextBox
    Friend WithEvents BANCO_A_DEPOSITARTextBox As System.Windows.Forms.TextBox
    Friend WithEvents NoCUENTATextBox As System.Windows.Forms.TextBox
    Friend WithEvents TelefonoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CelularTextBox As System.Windows.Forms.TextBox
    Friend WithEvents DireccionTextBox As System.Windows.Forms.TextBox
    Friend WithEvents btnGuardar As System.Windows.Forms.Button
    Friend WithEvents FECHA_INGRESODateTimePicker As System.Windows.Forms.DateTimePicker
End Class
