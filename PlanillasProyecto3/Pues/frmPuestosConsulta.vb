﻿Public Class frmPuestosConsulta

    Private Sub PuestosBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PuestosBindingNavigatorSaveItem.Click
        Me.Validate()
        Me.PuestosBindingSource.EndEdit()
        Me.TableAdapterManager.UpdateAll(Me.Planilla2DataSet)

    End Sub

    Private Sub frmPuestosConsulta_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'Planilla2DataSet.Puestos' table. You can move, or remove it, as needed.
        Me.PuestosTableAdapter.Fill(Me.Planilla2DataSet.Puestos)

    End Sub
End Class